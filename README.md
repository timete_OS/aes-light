AES-light
===================


AES-light is useful if you ever need to implement some basic/unsecure cypher, but you do not want to have yet another depedenciy for your project.

It allows to encrypt 256 bits objects with a known key. The basic functions looks like this. Feel free to copy/paste them directly. You can verify everything is working as intended in the main and only file of this project.



```Rust
fn chiffre(content: &[u8; 32], key: &[u8]) -> [u8; 32] {
    let shift_box: Vec<u8> = (0..=255).map(|x: u8| x.overflowing_add(7).0).collect();

    let mut block = content.to_owned();
    for _ in 0..16 {
        // linear operation
        block.iter_mut().for_each(|x| *x = shift_box[*x as usize]);
        // non linear substitution + using the key
        block.iter_mut().zip(key.iter()).for_each(|(x, y)| *x = PERMUTATION_BOX[(*x ^ y) as usize]);
    }

    block
}

fn dechiffre(cipher: &[u8; 32], key: &[u8]) -> [u8; 32] {
    let shift_box: Vec<u8> = (0..=255).map(|x: u8| x.overflowing_sub(7).0).collect();

    let mut block = cipher.clone();
    for _ in 0..16 {
        block.iter_mut().zip(key.iter()).for_each(|(x, y)| *x = INV_PERMUTATION_BOX[*x as usize] ^ y);
        block.iter_mut().for_each(|x| *x = shift_box[*x as usize]);
    }

    block
}
```


This is obviously not an implementation of AES. I just needed a quick symetric encryption algorithm, and utilized the core principles of AES:
- One non linear permutation
- XOR with the key at every pass
- Some linear transformation
- Multiple passes

Yes, I know right now I am doing the permutations at the bytes level, wich do not make a lot of sense. I don't know how insecure it is, but for now the only attacker I have is a friend of mine who is dumber than me.

## Usage

The two public functions can cypher and decypher arbitrary long u8 vectors. Use those

```rs
// 256 bits for the key
let key = "zPh6RrLsvUrXcGSHmZZWVZc75QWyU8Dt".as_bytes();
let data = "je suis un message bien en clair, et moi je suis un massage thailandais".as_bytes().to_owned();

let cipher = cypher(&data, key);
let res = decypher(&cipher, key);

assert!(data == decyphered_data);
```