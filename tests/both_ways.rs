use std::str;
// use base64::{engine::general_purpose, Engine as _};

use chiffre::{cypher, decypher};

const KEY: &[u8] = "zPh6RrLsvUrXcGSHmZZWVZc75QWyU8Dt".as_bytes(); // 256 bits for the key
const MESSAGES: [&str; 5] = [
    "a short message",
    "je suis un message bien en clair, et moi je suis un massage thai", // exactly 32 bytes
    "je suis un message bien en clair, et moi je suis un massage thailandais",
    "yet another message, but exactly one byte longuer than the batch size__!",
    "brocolis🥦" // unicode
];

#[test]
fn should_be_reversible() {

    for message in MESSAGES {
        println!("testing : {message}");
        let data = message.as_bytes().to_owned();

        let cipher = cypher(&data, KEY);
        let res = decypher(&cipher, KEY);

        let decyphered_message = str::from_utf8(&res).unwrap();
        assert!(message == decyphered_message);
    }
    // let passcode = general_purpose::STANDARD.encode(&cipher);
    // println!("chiffré : \n{}", passcode);

    // println!("dechiffre : ");
}



// #[test]
// #[should_panic]
// fn should_panic_rec() {
//     let (n, k) = (30, 51);
//     let _it = arrangements_rec(n, k);
// }

// #[test]
// #[should_panic]
// fn should_panic_iter() {
//     let (n, k) = (30, 51);
//     arrangements(n, k).next();
// }