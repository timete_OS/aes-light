use konst::iter;

const PERMUTATION_BOX: [u8; 256] = [255, 2, 3, 100,  64, 228,  34, 214, 233,  94, 108, 114, 152,
71,  40,   123,  83, 150, 120, 177,  68, 176, 209, 170, 200,  91,
127, 119, 153,   9, 215, 174,  44,  53, 131, 187,  48, 185, 102,
82, 208, 230, 243,  29, 201,  45,   6,  62,  38, 135,  63, 222,
219,  86,  32, 178, 159,  93,  31,  28,  67, 139, 193, 164,   4,
84, 229, 205, 239,  66,  36, 191,  58, 148,  79, 195, 107,  24,
138, 251,  98,  27, 121,  20, 105, 236,  77, 155,  26, 145,  51,
39, 190, 168, 126, 237, 223, 157, 154,  75, 244, 172, 192, 133,
122, 202,  19,  33, 130, 181, 216, 169, 87, 249,  80,  54, 204,
74,  70,  96, 188, 171, 218, 101, 182, 186, 203,  52,  95,  76,
254, 207, 147,   8,  42,  65,  23, 140,   7, 211, 194, 158,   248,
16,  41, 212,  78,  22,  61, 180, 104,   163,  50,  15, 240, 117,
128, 247, 189, 166, 206, 134,  47,  37, 132, 198, 160, 226, 115,
231, 253, 141, 109, 111,  55, 161,  10, 146,  13,  59,  60,  25,
99,  17,  43, 110, 199, 238, 245, 136,  85, 224, 234, 142, 235,
173, 210,  69,  81,  18,  89, 103, 225, 184, 232,  73, 183, 241,
11, 113, 246,   5, 116, 162, 197,  12,  49, 118, 143, 129,  57,
90,  14, 165, 124, 156,   1, 112,  35,  21, 137, 217,  97, 149,
175, 106,  92, 144, 125, 179, 242,  88, 196,  30,  46, 252, 151,
227, 167,  56, 250,  72, 220, 221, 213, 0];

const fn inv(x: u8) -> u8 {
    let mut i = 0;
    while PERMUTATION_BOX[i] != x {
        i += 1;
    }
    i as u8
}

const INV_PERMUTATION_BOX: [u8; 256] = iter::collect_const!(u8 => 0..=255, map(inv));

const SHIFT: u8 = 7;
const ROUNDS: usize = 16;
const CHUNK_SIZE: usize = 32;

fn chiffre(content: &[u8; CHUNK_SIZE], key: &[u8]) -> [u8; CHUNK_SIZE] {
    let shift_box: Vec<u8> = (0..=255).map(|x: u8| x.overflowing_add(SHIFT).0).collect();

    let mut block = content.to_owned();
    for _ in 0..ROUNDS {
        // linear operation
        block.iter_mut().for_each(|x| *x = shift_box[*x as usize]);
        // non linear substitution + using the key
        block.iter_mut().zip(key.iter()).for_each(|(x, y)| *x = PERMUTATION_BOX[(*x ^ y) as usize]);
    }

    block
}

fn dechiffre(cipher: &[u8; CHUNK_SIZE], key: &[u8]) -> [u8; CHUNK_SIZE] {
    let shift_box: Vec<u8> = (0..=255).map(|x: u8| x.overflowing_sub(SHIFT).0).collect();

    let mut block = cipher.clone();
    for _ in 0..ROUNDS {
        block.iter_mut().zip(key.iter()).for_each(|(x, y)| *x = INV_PERMUTATION_BOX[*x as usize] ^ y);
        block.iter_mut().for_each(|x| *x = shift_box[*x as usize]);
    }

    block
}

pub fn cypher(message: &Vec<u8>, key: &[u8]) -> Vec<u8> {
    let chunk_iterator = message.chunks_exact(CHUNK_SIZE);
    let mut res: Vec<u8> = Vec::with_capacity(message.len());
    let mut h: [u8; CHUNK_SIZE] = [0; CHUNK_SIZE];
    let remainder = chunk_iterator.remainder();

    for chunk in chunk_iterator {
        h.copy_from_slice(chunk);
        let i = chiffre(&h, key);
        res.extend(i);
    }

    // add necessary padding
    if remainder.len() > 0 {
        let mut padding: [u8; CHUNK_SIZE] = [0; CHUNK_SIZE];
        for (index, &value) in remainder.into_iter().enumerate() {
            padding[index] = value;
        };
        res.extend(chiffre(&padding, key));
    }

    res
}

pub fn decypher(cypher: &Vec<u8>, key: &[u8]) -> Vec<u8> {
    let chunk_iterator = cypher.chunks_exact(CHUNK_SIZE);
    let mut res: Vec<u8> = Vec::with_capacity(cypher.len());
    let mut h: [u8; CHUNK_SIZE] = [0; CHUNK_SIZE];

    for chunk in chunk_iterator {
        h.copy_from_slice(chunk);
        res.extend(dechiffre(&h, key));
    }

    // remove padding
    while match res.last() {
        Some(&v) if v == 0 => true,
        _ => false
    } { res.pop(); }

    res
}
