use std::str;
use base64::{engine::general_purpose, Engine as _};

use chiffre::{cypher, decypher};

pub fn main() {
    let key = "zPh6RrLsvUrXcGSHmZZWVZc75QWyU8Dt".as_bytes(); // 256 bits for the key
    let message = "je suis un message bien en clair, et moi je suis un massage thailandais";
    let data = message.as_bytes().to_owned();

    println!("message of lenght {}:\n{}", message.len(), message);
    let cipher = cypher(&data, key);
    let base64_cipher = general_purpose::STANDARD.encode(&cipher);
    println!("chiffré : \n{}", base64_cipher);

    println!("dechiffre : ");

    let res = decypher(&cipher, key);
    println!("{}", str::from_utf8(&res).unwrap());
}